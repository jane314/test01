| Date 	| Change 	|
|-	|-	|
| 2021-03-21 	| Multiple elements can be dragged, update `DiGraph` class and `SvgGraph.vue` to use the `DiGraph` class as a a model. 	|
| 2021-03-20 	| Elements can be added, single element can be dragged 	|


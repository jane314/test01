/**
 * @typedef Different type of settings we can specify for a vertex. Associated with different UI form input elements.
 */
type SettingType = string | boolean | number;

/**
 * @typedef A Setting consists of a name, a description, and a value
 */
interface ISetting {
	name: string;
	description: string;
	value: boolean | number | string;
}

/**
 * @typedef A Setting Template is a list of settings!
 */
interface ISettingTemplate {
	[id: number]: ISetting;
}

/**
 * @typedef A DiGraph's associated menu of possible setting templates to choose from
 */
export interface ISettingTemplateOptions {
	[settingTemplateName: string]: () => ISettingTemplate;
}

/**
 * @typedef A Vertex consists of an ID number for unique identification, x & y coords and color for graphical display, a Setting Template, and Vertexs pointed to and pointed from. There is no separate data type for edges / arrows - that information is contained within Vertex objects themselves.
 */
interface IVertex {
	id: number;
	settingList: ISettingTemplate;
	// We use id numbers for vertex lookups
	sourceOf: number[];
	targetOf: number[];
	x: number;
	y: number;
}

/**
 * @typedef An arrow has a source and a target. They are represented by the id of source and target.
 */
interface IArrow {
	source: number;
	target: number;
}

/**
 * @class Represents a directed graph on the Javascript side.
 * @template ComputeType It is common for a graph to be associated with a computed value. This is the type of that value.
 */
export abstract class DiGraph<ComputeType> {
	public vertices: { [id: number]: IVertex } = [];
	public rootVertices: { [id: number]: IVertex } = [];
	public arrows: IArrow[] = [];
	public settingTemplateOptions: ISettingTemplateOptions;
	private maxID = 0;

	public constructor(settingTemplateOptions: ISettingTemplateOptions) {
		this.settingTemplateOptions = settingTemplateOptions;
	}

	/**
	 * @function Computes value from subtree of the graph.
	 * @param vertex Root of a subtree of the graph.
	 * @returns ComputeType - The value associated with the subtree with `vertex` as its root.
	 */
	protected abstract followArrow(vertex: IVertex): ComputeType;

	/**
	 * @function Adds a new vertex to the graph.
	 * @param x SVG x-coordinate of new vertex, gotten from click event.
	 * @param y SVG y-coordinate of new vertex, gotten from click event.
	 * @param settingList Setting template to assign the new vertex. Usually a default value if we're adding a vertex.
	 */
	public addVertex(
		settingTemplateName: string,
		x: number,
		y: number
	): IVertex | Error {
		if (
			Object.keys(this.settingTemplateOptions).indexOf(settingTemplateName) ==
			-1
		) {
			return Error(
				`The following invalid setting template name was passed: ${settingTemplateName}`
			);
		}
		const newVertex: IVertex = {
			id: this.maxID,
			settingList: this.settingTemplateOptions[settingTemplateName](),
			sourceOf: [],
			targetOf: [],
			x: x,
			y: y,
		};
		this.vertices[this.maxID] = newVertex;
		this.rootVertices[this.maxID] = newVertex; // you're a root vertex until someone points at you
		this.maxID += 1;
		return newVertex;
	}

	/**
	 * @function Removes a vertex from the graph.
	 * @param id ID number of vertex to remove.
	 */
	public removeVertex(id: number): void {
		delete this.vertices[id];
		delete this.rootVertices[id];
	}

	/**
	 * @function Adds an arrow to the graph.
	 * @param sourceId Source vertex's id number.
	 * @param targetId Target vertex's id number.
	 */
	public addArrow(sourceId: number, targetId: number) {
		if (sourceId == targetId) {
			return;
		}
		if (
			this.vertices[sourceId] != undefined &&
			this.vertices[targetId] != undefined &&
			this.vertices[sourceId].sourceOf.indexOf(targetId) == -1
		) {
			this.vertices[sourceId].sourceOf.push(targetId);
			this.vertices[targetId].targetOf.push(sourceId);
			this.arrows.push({ source: sourceId, target: targetId });
		}
	}

	/**
	 * @function Creates JSON serialization of a DiGraph object.
	 */
	public toJSON(): string {
		return JSON.stringify({
			vertexList: this.vertices,
			rootvertexList: Object.keys(this.rootVertices),
			settingTemplateOptions: this.settingTemplateOptions,
			maxID: this.maxID,
		},null,4);
	}
}

/**
 * @class A test implementation of the DiGraph class.
 */
export class MyGraph extends DiGraph<number> {
	public settingTemplateOptions: ISettingTemplateOptions = {
		default: () => [
			{
				name: 'Name',
				description: 'Name on your BIRTH CERTIFICATE, nothing else please',
				value: 'Jimmy',
			},
			{
				name: 'Cool?',
				description: 'Are you cool?',
				value: false,
			},
			{
				name: 'Number',
				description: 'Your lucky number',
				value: 8779,
			},
		],
	};
	public followArrow(vertex: IVertex): number {
		return vertex.sourceOf
			.map(id => this.vertices[id])
			.map(this.followArrow)
			.reduce((a: number, b: number) => a + b);
	}
}

/*
 */
